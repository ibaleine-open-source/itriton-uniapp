import config from '../config/index'

/**
 * 同步缓存相关信息
 */
export const getStorageInfo = () => {
	return uni.getStorageInfoSync()
}

/**
 * 设置缓存
 * @param {string}	k key
 * @param {any}	v value    
 */
export const setStorage = (k, v) => {
	const key = getStorageKey(k)
	uni.setStorageSync(key, v)
}

/**
 * 获取缓存
 * @param {string}	k key 
 */
export const getStorage = (k) => {
	const key = getStorageKey(k)
	return uni.getStorageSync(key)
}

/**
 * 删除缓存
 * @param {string}	k key 
 */
export const removeStorage = (k) => {
	const key = getStorageKey(k)
	return uni.removeStorageSync(key)
}

/**
 * 清空缓存
 */
export const clearStorage = () => {
	return uni.clearStorageSync()
}

/**
 * 获取缓存key
 * @param {string}  k reset key
 */
const getStorageKey = (k) => {
	return `${config['name']}_${k}`.toUpperCase()
}

export default {
	getStorageInfo,
	setStorage,
	getStorage,
	removeStorage,
	clearStorage
}