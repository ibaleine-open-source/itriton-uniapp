const {
	setStorage,
	getStorage,
	removeStorage,
	clearStorage
} = require('./storage.js')
const config = require('../config/index')

let dtime = '_deadtime'

/**
 * 设置缓存
 * @param {string}	k key
 * @param {any}	v	value
 * @param {number}	t time(second)      
 */
export const setCache = (k, v, t) => {
	const key = getCacheKey(k)
	setStorage(key, v)
	let seconds = parseInt(t)
	if (seconds > 0) {
		let timestamp = Date.parse(new Date())
		timestamp = timestamp / 1000 + seconds
		setStorage(`${key}${dtime}`.toUpperCase(), `${timestamp}`)
	} else {
		removeStorage(`${key}${dtime}`.toUpperCase())
	}
}

/**
 * 获取缓存
 * @param {string}	k key
 * @param {string}  def  可选参数，表示无缓存数据时返回值（支持字符串、json、数组、boolean等）
 */
export const getCache = (k, def) => {
	const key = getCacheKey(k)
	const deadtime = parseInt(getStorage(`${key}${dtime}`.toUpperCase()))
	if (deadtime) {
		if (parseInt(deadtime) < Date.parse(new Date()) / 1000) {
			if (def) {
				return def;
			} else {
				return
			}
		}
	}
	const res = getStorage(key);
	if (res) {
		return res
	} else {
		return def
	}
}

/**
 * 移除cache
 * @param {string}  k key
 */
export const removeCache = (k) => {
	const key = getCacheKey(k)
	removeStorage(key)
	removeStorage(`${key}${dtime}`.toUpperCase())
}

/**
 * 清除cache
 */
export const clearCache = () => {
	clearStorage()
}

/**
 * 获取缓存key
 * @param {string} k key  
 */
const getCacheKey = (k) => {
	return `${k}`.toUpperCase()
}

export default {
	setCache,
	getCache,
	removeCache,
	clearCache
}