import config from './libs/config/index.js'
import color from './libs/config/color.js'
import common from './libs/utils/common.js'
import clipboard from './libs/utils/clipboard.js'
import cache from './libs/utils/cache.js'
import storage from './libs/utils/storage.js'
const $itu = {
	config,
	color,
	...common,
	...clipboard,
	...storage,
	...cache,
}

// $itu挂载到uni对象上
uni.$itu = $itu

const install = Vue => {
	Vue.prototype.$itu = $itu
}

export default {
	install
}