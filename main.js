import Vue from 'vue'
import App from './App'
import ITRITON_UNI from "itriton-ui"
import mixinCommon from '@/mixins/common.js'
import mixinIndex from '@/mixins/index.js'
// 引入dayjs
import dayjs from 'dayjs'
Vue.prototype.$dayjs = dayjs

Vue.config.productionTip = false
Vue.use(ITRITON_UNI)
Vue.mixin(mixinCommon)
Vue.mixin(mixinIndex)

App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
